package xa.batch331.lookup_service.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xa.batch331.lookup_service.models.Lookup;
import xa.batch331.lookup_service.repositories.LookupRepo;

import java.util.List;

@RestController
@RequestMapping("/apiv2")
public class LookupController {

    @Autowired
    private LookupRepo lookupRepo;

    @GetMapping("/lookup")
    public ResponseEntity<List<Lookup>> getAllLookup(){
        try {
            List<Lookup> lookup = this.lookupRepo.findAll();
            return new ResponseEntity<>(lookup, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
