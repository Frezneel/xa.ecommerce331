package xa.batch331.lookup_service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xa.batch331.lookup_service.models.Lookup;

@Repository
public interface LookupRepo extends JpaRepository<Lookup, Long> {
}
