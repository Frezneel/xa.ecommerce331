package xa.batch331.kafka_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import xa.batch331.kafka_project.kafkatest.CategoryConsumer;
import xa.batch331.kafka_project.kafkatest.CategoryProducer;

@SpringBootApplication
public class KafkaProjectApplication {

	public static void main(String[] args) {
		CategoryProducer.categoryProducer();
		CategoryConsumer.categoryConsumer();
		SpringApplication.run(KafkaProjectApplication.class, args);
	}


}
