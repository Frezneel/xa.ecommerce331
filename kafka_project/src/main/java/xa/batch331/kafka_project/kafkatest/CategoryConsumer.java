package xa.batch331.kafka_project.kafkatest;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Collections;
import java.util.Properties;

public class CategoryConsumer {
    public static void categoryConsumer(){
        Properties prop = new Properties();
        prop.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        prop.put(ConsumerConfig.GROUP_ID_CONFIG,"category_group");
        prop.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        prop.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<String, String> _categoryConsumer = new KafkaConsumer<>(prop);

        _categoryConsumer.subscribe(Collections.singleton("category"));

        try {
            while (true){
                ConsumerRecords<String, String> categoryRecord = _categoryConsumer.poll(100);
                for (ConsumerRecord<String, String> record : categoryRecord) {
                    System.out.println("Key: " + record.key() + ", value: " + record.value());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            _categoryConsumer.close();
        }
    }
}
