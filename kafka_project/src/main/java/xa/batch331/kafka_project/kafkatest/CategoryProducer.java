package xa.batch331.kafka_project.kafkatest;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.errors.SerializationException;

import java.util.Properties;

public class CategoryProducer {

    public static void categoryProducer(){
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer<String, String> _categoryProduser = new KafkaProducer<>(properties);

        try {
            String topic = "category";
            String key = "price";
            String value = "20000";

            ProducerRecord<String, String> record = new ProducerRecord<>(topic, key, value);

            _categoryProduser.send(record);
            System.out.println("Message has sended to topic : " + topic);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            _categoryProduser.close();
        }

    }
}
