package xa.batch331.category_consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CategoryConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CategoryConsumerApplication.class, args);
	}

}
