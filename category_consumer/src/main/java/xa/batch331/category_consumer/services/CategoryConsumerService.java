package xa.batch331.category_consumer.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import xa.batch331.category_consumer.models.Category;
import xa.batch331.category_consumer.repositories.CategoryRepository;

import java.util.HashMap;
import java.util.Map;

@Service
public class CategoryConsumerService {

    private final Logger logger = LoggerFactory.getLogger(CategoryConsumerService.class);
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private CategoryRepository categoryRepository;

    @KafkaListener(topics = "category", groupId = "group_category")
    public void consumeCategory(String message) throws JsonMappingException, JsonProcessingException {
        Category category = objectMapper.readValue(message, Category.class);
        this.categoryRepository.save(category);

        logger.info(String.format(">>>>>>>> Consume Category  -> %s", message));
    }

}
