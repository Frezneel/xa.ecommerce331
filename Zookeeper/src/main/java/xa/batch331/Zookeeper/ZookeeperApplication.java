package xa.batch331.Zookeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
public class ZookeeperApplication {

	@GetMapping
	public String home(){
		return "Hello world";
	}

	public static void main(String[] args) {
		SpringApplication.run(ZookeeperApplication.class, args);
	}

}
