package xa.batch331.Zookeeper.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ZooController {

    @Value("${spring.kafka.zookeeper.dataDir}")
    private String dataDirProperty;

    @GetMapping("/datadir")
    public String getExampleProperty(){
        return "dataDir property: " + this.dataDirProperty;
    }
}
