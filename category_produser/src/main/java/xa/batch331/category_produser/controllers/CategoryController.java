package xa.batch331.category_produser.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.category_produser.models.Category;
import xa.batch331.category_produser.models.CategoryDTO;
import xa.batch331.category_produser.repositories.CategoryRepo;
import xa.batch331.category_produser.services.CategoryProducerService;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class CategoryController {

    private final CategoryProducerService categoryProducerService;
    @Autowired
    private CategoryRepo categoryRepo;

    public CategoryController(CategoryProducerService categoryProducerService) {
        this.categoryProducerService = categoryProducerService;
    }

    private static JsonNode removeKey(JsonNode jsonNode, String keyToRemove) {
        if (jsonNode.isObject()) {
            ObjectNode objectNode = (ObjectNode) jsonNode;

            objectNode.remove(keyToRemove);

            return objectNode;
        } else {
            return jsonNode;
        }
    }

    @PostMapping(value = "/producecategory")
    public ResponseEntity<Category> sendMessageToKafka(@RequestBody Category category){
        this.categoryRepo.save(category);
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setName(category.getName());
        categoryDTO.setDescription(category.getDescription());

        ObjectMapper obj = new ObjectMapper();
        try {
            String jsonString = obj.writeValueAsString(categoryDTO);
            this.categoryProducerService.sendMessage(jsonString);

            return new ResponseEntity<>(category, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
