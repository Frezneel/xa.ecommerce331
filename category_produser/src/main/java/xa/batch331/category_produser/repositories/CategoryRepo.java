package xa.batch331.category_produser.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xa.batch331.category_produser.models.Category;

public interface CategoryRepo extends JpaRepository<Category, Long> {

}
