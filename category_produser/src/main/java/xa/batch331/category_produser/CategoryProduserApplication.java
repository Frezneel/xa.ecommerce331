package xa.batch331.category_produser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CategoryProduserApplication {

	public static void main(String[] args) {
		SpringApplication.run(CategoryProduserApplication.class, args);
	}

}
