package xa.batch331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import xa.batch331.Services.CategoryService;
import xa.batch331.models.Category;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    private static String UPLOADED_FOLDER = "D:/Bootcamp Java/Pra-Placement/javafile/";

    @GetMapping("")
    public ModelAndView getCategory(){
        ModelAndView view = new ModelAndView("category/index");
        List<Category> category = this.categoryService.getAllCategory();
        view.addObject("categoryData", category);
        return view;
    }

    @GetMapping("/{id}")
    public ModelAndView getCategoryId(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("category/category");
        Optional<Category> category = this.categoryService.getCategoryById(id);
        view.addObject("categoryData", category.get());
        return view;
    }

    @GetMapping("/form")
    public ModelAndView form(){
        ModelAndView view = new ModelAndView("category/form");
        Category category = new Category();
        view.addObject("category",category);
        return view;
    }

    @PostMapping("/save")
    public ModelAndView save(@ModelAttribute Category category, BindingResult result,
                             @RequestParam("filepath")MultipartFile file) throws Exception{
        if (!result.hasErrors()){
            try {
                if (file.getOriginalFilename() != ""){
                    //upload file fisik ke local folder
                    //byte[] bytes = file.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());

                    //save file to byte
                    byte[] fileContent = file.getBytes();

                    //set file path to column FilePath
                    category.setFilePath("/files/" + file.getOriginalFilename());
                    //set File content (byte[]) to column File Content
                    category.setFileContent(fileContent);
                    //save category entitiy
                    this.categoryService.simpanCategory(category);
//                    Path path2 = Paths.get().;
                    Files.write(path,fileContent);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return new ModelAndView("redirect:/category");
    }

    @PutMapping("/editform/{id}")
    public ModelAndView edit(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("category/form");
        Category category = this.categoryService.getCategoryById(id).get();
        view.addObject("category",category);
        return view;
    }

    @GetMapping("/deleteform/{id}")
    public ModelAndView delete(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("category/delform");
        Category category = this.categoryService.getCategoryById(id).get();
        view.addObject("category", category);
        return view;
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deleteItem(@PathVariable("id") Long id){
        this.categoryService.deleteCategory(id);
        return new ModelAndView("redirect:/category");
    }

    @GetMapping("/getByteFile/{id}")
    public ModelAndView getByteFile(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("category/getfile");
        byte[] fileContent = this.categoryService.readFileContent(id);
        view.addObject("fileContent", fileContent);
        return view;
    }
}
