package xa.batch331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import xa.batch331.Services.CategoryService;
import xa.batch331.Services.ProductService;
import xa.batch331.models.Category;
import xa.batch331.models.Product;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("product")
public class ProductController {

    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public ModelAndView getProduct(){
        ModelAndView view = new ModelAndView("product/index");
        List<Product> products = this.productService.getAllProduct();
        view.addObject("productData",products);
        return view;
    }

    @GetMapping("/form")
    public ModelAndView from(){
        ModelAndView view = new ModelAndView("product/form");
        List<Category> categories = this.categoryService.getAllCategory();
        Product product = new Product();
        view.addObject("categoryData",categories);
        view.addObject("product",product);
        return view;
    }

    @PostMapping("/save")
    public ModelAndView save(Product product, BindingResult bindingResult){
        if (!bindingResult.hasErrors()){
            this.productService.saveProduct(product);
        }
        return new ModelAndView("redirect:/product");
    }

    @GetMapping("/editform/{id}")
    public ModelAndView edit(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("product/form");
        List<Category> categories = this.categoryService.getAllCategory();
        Optional<Product> product = this.productService.getProductById(id);
        view.addObject("categoryData",categories);
        view.addObject("product",product.get());
        return view;
    }

    @GetMapping("/deleteform/{id}")
    public ModelAndView delete(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("product/delform");
        Product product = this.productService.getProductById(id).get();
        view.addObject("productData", product);
        return view;
    }

}
