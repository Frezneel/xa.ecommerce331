package xa.batch331.Services;

import com.fasterxml.jackson.databind.cfg.MapperBuilder;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import xa.batch331.models.Category;
import xa.batch331.models.CategoryDTO;
import xa.batch331.repositories.CategoryRepo;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepo categoryRepo;

    public List<Category> getAllCategory(){
        return this.categoryRepo.findAll();
    }

    public List<Map<String, Object>> getCategoryValue(){
        return this.categoryRepo.getCategoryValue();
    }

    public Optional<Category> getCategoryById(Long id){
        return this.categoryRepo.findById(id);
    }

    public void simpanCategory(Category category){
        this.categoryRepo.save(category);
    }

    public void deleteCategory(Long id){
        this.categoryRepo.deleteById(id);
    }

    //konversi dari entity ke DTO
    public CategoryDTO convertToDTO(Category category){
        ModelMapper modelMapper = new ModelMapper();
        CategoryDTO categoryDTO = modelMapper.map(category, CategoryDTO.class);
        return categoryDTO;
    }

    public CategoryDTO convertToDTO(Long id){
        Category categoryDTO = this.categoryRepo.findById(id).get();
        return convertToDTO(categoryDTO);
    }

    public byte[] readFileContent(Long id){
        Category category = this.categoryRepo.findById(id).orElse(null);
        if (category != null){
            return category.getFileContent();
        }else {
            return null;
        }
    }
}
